package com.vivek.vertx.SenderVerticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import main.com.vivek.common.Starter;

import java.util.logging.Logger;

public class LaucherVertical extends AbstractVerticle {
    private final static Logger LOGGER =  Logger.getLogger(Starter.class.getName());

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        deployHelper(SenderVerticle.class.getName());

    }

    private Future<Void> deployHelper(String name) {
        final Future<Void> future = Future.future();
        vertx.deployVerticle(name, res -> {
            if (res.failed()) {
                //log.info("Failed to deploy verticle " + name);
                future.fail(res.cause());
            } else {
                //log.info("Deployed verticle " + name);
                future.complete();
            }
        });
        return future;
    }
}
