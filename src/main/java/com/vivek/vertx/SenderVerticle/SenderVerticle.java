package com.vivek.vertx.SenderVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import main.com.vivek.common.Constants;
import main.com.vivek.common.Starter;

import java.util.logging.Logger;


public class SenderVerticle extends AbstractVerticle {
    private final static Logger LOGGER =  Logger.getLogger(Starter.class.getName());
    @Override
    public void start(Future<Void> future) throws Exception {
        final Router router = Router.router(vertx);
        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response.putHeader("content-type", "text/html")
                    .end("<h1>Hello World Example Vivek 2</h1>");
        });
        router.post("/send/:message").handler(this::sendMessage);
        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(config()
                        .getInteger("http.server.port", Constants.PORT), result -> {
                    if (result.succeeded()) {
                        future.complete();
                    } else {
                        future.fail(result.cause());
                    }
                });
        LOGGER.info("Started Sender Thread:  " + Thread.currentThread().getId() +
                "Is Clustered: " + vertx.isClustered());
    }

    private void sendMessage(RoutingContext routingContext){
        final EventBus eventBus = vertx.eventBus();
        final String message = routingContext.request().getParam("message");
        /*eventBus.send(Constants.ADDRESS, message, reply -> {
            if (reply.succeeded()) {
                System.out.println("Received reply: " + reply.result().body());
            } else {
                System.out.println("No reply");
            }
        });*/
        eventBus.publish(Constants.ADDRESS, message);
        System.out.println("Current Thread Id {} Is Clustered {} " + Thread.currentThread().getId()
                + vertx.isClustered());
        routingContext.response().end(message);
    }

}
