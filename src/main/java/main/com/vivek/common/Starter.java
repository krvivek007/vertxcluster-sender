package main.com.vivek.common;

import com.vivek.vertx.SenderVerticle.LaucherVertical;
import io.vertx.core.Vertx;

import java.util.logging.Logger;

public class Starter {
    private final static Logger LOGGER =  Logger.getLogger(Starter.class.getName());

    public static void main(String[] args){
        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new LaucherVertical(), res -> {
            if (res.succeeded()) {
                LOGGER.info("Deployment id is: " + res.result());
            } else {
                LOGGER.info("Deployment failed!");
            }
        });
    }
}